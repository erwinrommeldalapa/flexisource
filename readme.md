# Flexisource Exam

RESTful API and Data Importer CRON Job.

### Tools and programming language used

PHP (Laravel Framework), virtual machine (Vagrant), nginx server ubuntu, bootstrap 4.0, HTML5, css, json, MySQL, Restlet Client DHC (for testing), git

### Library / package used

GuzzleHttp Client for http request

### Installation Requirements

* MySQL / MariaDB for database 
* Browser Chrome / Mozilla
* nginx / apache server

### Installaation

* Clone the repository and update the composer
* Add .env file and change the values of: 

DB_DATABASE=flexisource
DB_USERNAME=homestead
DB_PASSWORD=secret

execute the database migration "php artisan migrate"

## master or home {baseURL}

Here users can view the players list and details in table.

## API Endpoints / usage
select all the rows / data from the players table
{baseURL}/api/getPlayers
METHOD: GET

select the player based on player_id
{baseURL}/api/getPlayer/{player_id}
METHOD: GET
Sample usage: http://flexisource.test/api/getPlayer/2

## UnitTests Cases
Path tests/Unit/HelperTest.php
test the Helper function if the response data is valid XML (function testIsValidXml)

Path tests/Unit/PlayerApiTest.php
test the api endpoints that fetches the list of all players and fetch the specific player using player_id

## Importer
Path App/Jobs/CronImporter.php
Importer is API based it fetches the players list from this data provider
https://fantasy.premierleague.com/api/bootstrap-static/
and store a minimum of 100 player data into the database mysql.

* Set schedule to hourly update / execution

For Testing I have made a php artisan command to run the cron job.
Usage: php artisan hourlyupdate:importer

## Exam Requirements
Features:
Display a list of players - {baseURL} or home page

Click any player for a detailed view of their statistics - {baseURL} or home page (View Detail Button)

Data Importer - App/Jobs/CronImporter.php

Create 2 RESTful API endpoints.
1. Get the list of all players with only ID and Full Name. - {baseURL}/api/getPlayers (endpoint)

2. Get the full data object of a single Player. The object should contain at least but not limit to id, first
name, second name, form, total points, influence, creativity, threat, and ICT index. 

- {baseURL}/api/getPlayer/{player_id} (endpoint)


Unit Test the code you write. 
Path tests/Unit

 