<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('importer', ['middleware' => ['cors'], 'uses' => 'PlayerController@importPlayers']);
Route::get('getPlayers', ['middleware' => ['cors'], 'uses' => 'PlayerController@getPlayers']);
Route::get('getPlayer/{player_id}', ['middleware' => ['cors'], 'uses' => 'PlayerController@getPlayerData']);
