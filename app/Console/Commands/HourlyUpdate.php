<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HourlyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hourlyupdate:importer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute the importer every hour';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
     $client = new \GuzzleHttp\Client([
        'headers' => [ 'Accept' => 'application/json', 'Accept' => 
        'application/json', 'Content-Type' => 'application/json', 
        'Sec-Fetch-Site' => 'cross-site',
        'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
    ]]);
     try{
        $res = $client->request('GET', 'http://flexisource.test/api/importer');
    }catch(\Exception $e){

    }
}
}
