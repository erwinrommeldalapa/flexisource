<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;

class IndexController extends Controller
{
    //
	public function view(){
		$players = Player::orderbyDesc('id')->get();
		$data = ['players' => $players];
		return view('master')->with($data);
	}
}
