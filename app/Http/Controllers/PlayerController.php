<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Http\Helper;

class PlayerController extends Controller
{

	public function importPlayers(){
		$fetchIndexNo = 0;
		$fetchNumberOfData = 100;

		$client = new \GuzzleHttp\Client([
			'headers' => [ 'Accept' => 'application/json', 'Accept' => 
			'application/json', 'Content-Type' => 'application/json', 
			'Sec-Fetch-Site' => 'cross-site',
			'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
		]]);

		try{
			$res = $client->request('GET', 'https://fantasy.premierleague.com/api/bootstrap-static/');
			$jsonRes = json_decode($res->getBody(true));
			$players = Player::latest('id')->first();

			//check the latest player_id fetched by the cron job
			if($players){
				$fetchIndexNo = $players->player_id - 1;
			}
			//check the number of data available to fetch
			if($fetchNumberOfData > count($jsonRes->elements)){
				$fetchNumberOfData = count($jsonRes->elements) - $fetchNumberOfData;
			}

			//check and converts xml to json
			if(Helper::isValidXml(json_encode($jsonRes))){
				$jsonRes = json_encode(simplexml_load_string($xml));
			}

			for($i = $fetchIndexNo; $i<$fetchNumberOfData; $i++){
				//check if player exist and update the details
				$ifPlayerExist = Player::where('player_id', strval(json_decode(json_encode($jsonRes->elements[$i]->id))))->first();

				if($ifPlayerExist){
					$ifPlayerExist->details = json_encode($jsonRes->elements[0]);
					$ifPlayerExist->save();
				}else{
					$player = new Player;
					$player->name = strval(json_decode(json_encode($jsonRes->elements[$i]->first_name)));
					$player->player_id = strval(json_decode(json_encode($jsonRes->elements[$i]->id)));
					$player->details = json_encode($jsonRes->elements[0]);
					$player->save();
				}
			}

		}catch(\Exception $e){
			echo 'Message: ' .$e->getMessage();
		}
	}

	public function getPlayers(Request $request){
		if(!$request->method() == "GET"){
			return response(405);
		}

		$players = Player::select('name', 'player_id')->get();
		return json_decode($players);
	}

	public function getPlayerData(Request $request){
		if(!$request->method() == "GET"){
			return response(405);
		}

		$players = Player::where('player_id', $request->player_id)->get();
		return json_decode($players);
	}
}