<?php
namespace App\Http;

class Helper
{
	public static function isValidXml($content)
	{	
		$content = trim($content);
		if (empty($content)) {
			return false;
		}

		if (stripos($content, '<!DOCTYPE html>') !== false) {
			return false;
		}

		libxml_use_internal_errors(true);
		simplexml_load_string($content);
		$errors = libxml_get_errors();          
		libxml_clear_errors();  

		return empty($errors);
	}
}

?>