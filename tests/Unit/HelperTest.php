<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Helper;

class HelperTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIsValidXml()
    {
        $this->assertFalse(Helper::isValidXml('<!DOCTYPE html><html><body></body></html>'));
        $this->assertTrue(Helper::isValidXml('<?xml version="1.0" standalone="yes"?><root></root>'));
        $this->assertFalse(Helper::isValidXml(null));
        $this->assertFalse(Helper::isValidXml('<test>123123<test>'));
        $this->assertFalse(Helper::isValidXml(false));
    }
}
