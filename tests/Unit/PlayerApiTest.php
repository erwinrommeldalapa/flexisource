<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Helper;

class PlayerApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetPlayers()
    {
       $response = $this->json('GET', '/api/getPlayers');
       $response->assertStatus(200);

       $responseAsPost = $this->json('POST', '/api/getPlayers');
       $responseAsPost->assertStatus(405);

       $responseNotFound = $this->json('GET', '/api/getPlayer');
       $responseNotFound->assertNotFound();
   }

   public function testGetPlayerId()
   {
       $response = $this->json('GET', '/api/getPlayer/1');
       $response->assertStatus(200);
           
       $responseNoData = $this->json('GET', '/api/getPlayer/1203');
       $responseNoData->assertExactJson([])
                      ->assertStatus(200);

       $responseAsPost = $this->json('POST', '/api/getPlayer/1');
       $responseAsPost->assertStatus(405);

       $responseNotFound = $this->json('GET', '/api/getPlayers/1');
       $responseNotFound->assertNotFound();
   }
}