<!DOCTYPE html>
<html>
<html class="no-js" lang="en">
<meta https-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    @include ('includes.head')
</head>
<body>
    @include('content')
    <footer>
        @include ('includes.footer')
    </footer>
</body>
</html>
