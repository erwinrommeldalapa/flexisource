<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Action</th>
      <th scope="col">Name</th>
      <th scope="col">Player Id</th>
    </tr>
  </thead>
  <tbody>
    @foreach($players as $player)
    <tr>
     <td>
      <p>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample<?php echo $player->id ?>" aria-expanded="false">
          View Details
        </button>
      </p>
      <div class="collapse" id="collapseExample<?php echo $player->id ?>">
        <div class="card card-body">
          <?php $deets = json_decode(json_decode(json_encode($player->details))); ?>
          @foreach($deets as $key=>$value)
          {{ $key . ':' }}
          {{ $value }}
          <br>
          @endforeach
        </div>
      </div>
    </td>
    <td>{{$player->player_id}}</td>
    <td>{{$player->name}}</td>

  </tr>
  @endforeach
</tbody>
</table>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>